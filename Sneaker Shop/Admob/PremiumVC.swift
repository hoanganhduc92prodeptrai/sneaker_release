//
//  PremiumVC.swift
//  Kickboxing Master
//
//  Created by Hius on 1/7/21.
//

import UIKit
import StoreKit
import SwiftyStoreKit
import MBProgressHUD
import GoogleMobileAds
import CircleBar

enum PurchaseType: Int {
    case weekly
    case monthly
    case yearly
}
let PRODUCT_ID_YEARLY = "kickboxing.learn.kickbox"
let PRODUCT_SHARED_SECRET = "e565c762c6014b8c88f927314c06e504"
let PRODUCT_IDS = [PRODUCT_ID_YEARLY]

class PremiumVC: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    var purchaseType: PurchaseType = .yearly
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var btnUpgrade: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var premiumFontText: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var costFontText: UILabel!
    @IBOutlet weak var btnRestore: UIButton!
    
    @IBAction func btnSkipHandle(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnBackHandle(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnUpgradeHandle(_ sender: Any) {
        purchaseType = .yearly
        self.purchasePro(type: .yearly)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnBack.isHidden = true
        btnSkip.alpha = 0.8
        
        //        if PaymentManager.shared.isPurchase() {
        //            btnSkip.isHidden = true
        //        } else {
        //            btnSkip.isHidden = false
        //        }
        
        costFontText.text = "Only $4.99"
        
        //        backgroundView.bgOffWhite()
        //        backgroundView.setShadow(withRadius: 20)
        //        btnUpgrade.setShadow(withRadius: 10)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        AdMobManager.shared.loadBannerView(inVC: self)
    }
    
    
    @IBAction func restorePurchaseHandle(_ sender: Any) {
        showLoading()
        SwiftyStoreKit.restorePurchases(atomically: false) { results in
            self.hideLoading()
            if results.restoreFailedPurchases.count > 0 {
                print("Restore Failed: \(results.restoreFailedPurchases)")
            }
            else if results.restoredPurchases.count > 0 {
                for purchase in results.restoredPurchases {
                    // fetch content from your server, then:
                    if purchase.needsFinishTransaction {
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                }
                let dateFormatter: DateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MMM-dd HH:mm:ss"
                let date = Date()
                let dateString = dateFormatter.string(from: date)
                let interval = date.timeIntervalSince1970
                PaymentManager.shared.savePurchase()
                let fastCleanVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabbarPro") as! UITabBarController
                fastCleanVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                self.present(fastCleanVC, animated: true)
            }
            else {
                print("Nothing to Restore")
            }
        }
    }
    
    
    func purchasePro(type: PurchaseType) {
        var productId = PRODUCT_ID_YEARLY
        if type == .yearly {
            productId = PRODUCT_ID_YEARLY
        }
        self.showLoading()
        SwiftyStoreKit.purchaseProduct(productId, quantity: 1, atomically: false) { result in
            self.hideLoading()
            switch result {
            case .success(let product):
                // fetch content from your server, then:
                if product.needsFinishTransaction {
                    SwiftyStoreKit.finishTransaction(product.transaction)
                }
                let dateFormatter: DateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MMM-dd HH:mm:ss"
                let date = Date()
                let dateString = dateFormatter.string(from: date)
                let interval = date.timeIntervalSince1970
                PaymentManager.shared.savePurchase()
                print("Purchase Success: \(product.productId)")
                let fastCleanVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabbarPro") as! UITabBarController
                fastCleanVC.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
                self.present(fastCleanVC, animated: true)
            case .error(let error):
                switch error.code {
                case .unknown: print("Unknown error. Please contact support")
                case .clientInvalid: print("Not allowed to make the payment")
                case .paymentCancelled: break
                case .paymentInvalid: print("The purchase identifier was invalid")
                case .paymentNotAllowed: print("The device is not allowed to make the payment")
                case .storeProductNotAvailable: print("The product is not available in the current storefront")
                case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
                case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
                case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
                default: print((error as NSError).localizedDescription)
                }
            }
        }
    }
}
